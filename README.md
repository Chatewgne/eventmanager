# EventManager

Ce projet contient deux challenges :

## 1 - python_chall

Un module Python de stockage d'évènements avec TinyDB.

### Utilisation

```bash
cd python_chall/
python3 -m pip install requirements.txt
python3 test.py
```

Le module crée le fichier ```python_chall/data/events.json``` où  sont stockés les évènements.

## 2 - web_chall 

Une interface web avec une table CRUD en ReactJS pour la gestion des évènements. Cette application utilise le module du challenge 1.

### Utilisation

```bash
cd web_chall/
python3 -m pip install requirements.txt
python3 EventManager.py
```

L'application est disponible sur ```127.0.0.1:5000/EventManager```. Elle crée le fichier ```web_chall/data/events.json``` où  sont stockés les évènements.