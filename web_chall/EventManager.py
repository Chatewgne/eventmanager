#!/usr/bin/python3
# -*- coding: utf-8 -*-
# File name          : EventManager.py
# Author             : Celia Prat
# Date created       : Jul 24, 2020
# Date last modified : Feb 11, 2021
# Python Version     : 3.6.9

import json
from datetime import datetime

from flask import Flask, jsonify, request

from storage import DatetimeEventStore

app = Flask(__name__)
store = DatetimeEventStore()


@app.route("/EventManager")
def home():
    return app.send_static_file("index.html")


def fill_store():
    store.drop()
    store.store_event(
        datetime.strptime("2020-07-27 12:14:34", "%Y-%m-%d %H:%M:%S"),
        "Event 1",
    )
    store.store_event(
        datetime.strptime("2020-07-27 12:32:37", "%Y-%m-%d %H:%M:%S"),
        "Event 2",
    )
    store.store_event(
        datetime.strptime("2020-07-27 12:42:54", "%Y-%m-%d %H:%M:%S"),
        "Event 3",
    )
    store.store_event(
        datetime.strptime("2020-07-27 12:46:18", "%Y-%m-%d %H:%M:%S"),
        "Event 4",
    )
    store.store_event(
        datetime.strptime("2020-07-27 12:55:37", "%Y-%m-%d %H:%M:%S"),
        "Event 5",
    )


@app.route("/all_events")
def all_events():
    return json.dumps(store.get_all_events(raw=True))


@app.route("/delete", methods=["POST"])
def delete():
    event_id = request.json["event_id"]
    store.remove_event(event_id)
    return jsonify(isError=False, message="Success", statusCode=200), 200
    # todo error handling


@app.route("/create", methods=["POST"])
def create():
    date = datetime.strptime(request.json["datetime"], "%Y-%m-%d %H:%M:%S")
    description = request.json["description"]
    event_id = store.store_event(date, description)
    return (
        jsonify(isError=False, message="Success", id=event_id, statusCode=200),
        200,
    )
    # todo error handling


@app.route("/update", methods=["POST"])
def update():
    event_id = description = request.json["event_id"]
    date = datetime.strptime(request.json["datetime"], "%Y-%m-%d %H:%M:%S")
    description = request.json["description"]
    store.update_event(event_id, date, description)
    return (
        jsonify(isError=False, message="Success", id=event_id, statusCode=200),
        200,
    )
    # todo error handling


fill_store()
app.run(host="0.0.0.0")
