class NewEventRow extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            datetime: "2020-07-27 12:32:37",
            description: "Something happened",
        }  
    }

    createEvent() {
        var datetime_regex = /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])$/;
        if (this.state.datetime.match(datetime_regex)) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ "datetime": this.state.datetime, "description": this.state.description }),
            }
            fetch('/create', requestOptions).then(res => res.json()).then(data => this.props.onCreate(data.id,this.state.datetime,this.state.description));
            //todo error handling
        } else {
            alert("Please enter a valid datetime format : YYYY-MM-DD HH:MM:SS");
        }
    }

    getDatetime(e) {
        this.setState({datetime: e.target.value});
    }

    getDescription(e) {
        this.setState({description: e.target.value});
    }

    render() {
        return ( <tr>
                    <td></td>
                    <td><input type="text" value={this.state.datetime} onChange={ (e) => this.getDatetime(e)}/></td>
                    <td><input type="text" value={this.state.description} onChange={ (e) => this.getDescription(e)}/></td>
                    <td>
                    <button onClick={() => this.createEvent()} className="create" >Create</button>
                    </td>
                </tr>
                )
    }
}


class Row extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            datetime: this.props.datetime,     //Default to original event values. These might be updated later.
            description: this.props.description,
            show: true,
            update: false,
        }
    }

    deleteEvent() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ "event_id": this.props.id }),
        }
        fetch('/delete', requestOptions);
        this.setState({show: false});
        //todo error handling
    }

    updateEvent() {
        var datetime_regex = /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])$/;
        if (this.state.datetime.match(datetime_regex)) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({"event_id":this.props.id, "datetime": this.state.datetime, "description": this.state.description }),
            }
            fetch('/update', requestOptions);  //todo error handling
            this.setState({update: false})

        } else {
            alert("Please enter a valid datetime format : YYYY-MM-DD HH:MM:SS");
        }
    }

    getDatetime(e) {
        this.setState({datetime: e.target.value});
    }

    getDescription(e) {
        this.setState({description: e.target.value});
    }

    render() {
        return this.state.show ? 
            //If row is in "show" mode, display row
                this.state.update ?
                //If row is in "update" mode, display input boxes
                (<tr>
                    <td>{this.props.id}</td>
                    <td><input type="text" value={this.state.datetime} onChange={(e) => this.getDatetime(e)}/></td>
                    <td><input type="text" value={this.state.description} onChange={(e) => this.getDescription(e)}/></td>
                    <td>
                    <button className="save" onClick={() => this.updateEvent()} >Save</button>
                    <button className="delete" onClick={() => this.deleteEvent()}>Delete</button>
                    </td>
                </tr> ) 
                : //If row is in "read" mode, display default row
                (<tr>
                    <td>{this.props.id}</td>
                    <td>{this.state.datetime}</td>
                    <td>{this.state.description}</td>
                    <td>
                    <button className="update" onClick={() => this.setState({update: true})} >Update</button>
                    <button className="delete" onClick={() => this.deleteEvent()}>Delete</button>
                    </td>
                </tr> ) 
            //If row is in "hidden" mode, display nothing
            : null 
        }
}


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            rows: [],
        }
        this.addRow = this.addRow.bind(this);
        this.initRows();
      }

    addRow(event_id, datetime, description) {
        var new_rows = this.state.rows.concat(<Row id={event_id} datetime={datetime} description={description} />);   
        this.setState({rows: new_rows});
    }

    setRows(events){
        var new_rows = [];
        for (var i = 0; i < events.length; i++){
                var event = events[i].event;
                var event_id = events[i].key;
                new_rows.push(<Row id={event_id} datetime={event.datetime} description={event.description} />);    
        }
        this.setState({rows: new_rows});
    }

    initRows(){
        return fetch('/all_events').then(res => res.json()).then(events => this.setRows(events));
    }

    render() {
        return (
            <table>
            <thead>
            <tr>
                <th>Event ID</th>
                <th>Datetime</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                {this.state.rows}
                <NewEventRow onCreate={this.addRow}/> 
            </tbody>
            </table>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
  );