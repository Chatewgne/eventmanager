#!/usr/bin/python3
# -*- coding: utf-8 -*-
# File name          : storage.py
# Author             : Celia Prat
# Date created       : Jul 24, 2020
# Date last modified : Feb 11, 2021
# Python Version     : 3.6.9

from datetime import datetime

from tinydb import Query, TinyDB
from tinydb.operations import set


class DatetimeEventStore:
    class Event:
        def __init__(self, id, datetime, description):
            self.id = id
            self.datetime = datetime
            self.description = description

        @classmethod
        def from_json(cls, event_json):
            return cls(
                event_json.doc_id,
                datetime.strptime(event_json["datetime"], Utils.DATE_FORMAT),
                event_json["description"],
            )

        def __str__(self):
            return (
                str(self.id)
                + "] "
                + str(self.datetime)
                + " - "
                + self.description
            )

    def __init__(self):
        self.db = TinyDB("./data/events.json", create_dirs=True)

    def store_event(self, datetime, data):
        self.db.insert({"datetime": str(datetime), "description": data})

    def get_all_events(self):
        return self.sorted_events(
            map(lambda e: self.Event.from_json(e), self.db)
        )

    def sorted_events(self, event_list):
        return sorted(event_list, key=lambda e: e.datetime)

    def get_events(self, start, end):
        Event = Query()
        events = self.db.search(
            Event.datetime.test(Utils.is_between, start, end)
        )
        return self.sorted_events(
            map(lambda e: self.Event.from_json(e), events)
        )

    def get_event_by_id(self, id):
        return self.Event.from_json(self.db.get(doc_id=id))

    def update_event_data(self, id, data):
        self.db.update(set("description", data), doc_ids=[id])

    def update_event_datetime(self, id, datetime):
        self.db.update(set("datetime", str(datetime)), doc_ids=[id])

    def remove_event(self, id):
        self.db.remove(doc_ids=[id])

    def remove_events(self, start, end):
        Event = Query()
        events = self.db.search(
            Event.datetime.test(Utils.is_between, start, end)
        )
        ids = list(map(lambda e: e.doc_id, events))
        self.db.remove(doc_ids=ids)

    def drop(self):
        self.db.drop_tables()


class Utils:

    DATE_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

    @staticmethod
    def is_before(datetime_string, end):
        return datetime.strptime(datetime_string, Utils.DATE_FORMAT) <= end

    @staticmethod
    def is_after(datetime_string, start):
        return start <= datetime.strptime(datetime_string, Utils.DATE_FORMAT)

    @staticmethod
    def is_between(datetime_string, start, end):
        return Utils.is_after(datetime_string, start) & Utils.is_before(
            datetime_string, end
        )
