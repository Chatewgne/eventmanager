#!/usr/bin/python3
# -*- coding: utf-8 -*-
# File name          : test.py
# Author             : Celia Prat
# Date created       : Jul 24, 2020
# Date last modified : Jul 29, 2020
# Python Version     : 3.6.9

from datetime import datetime
from storage import DatetimeEventStore

store = DatetimeEventStore()
store.store_event(datetime.now(),"Event1")
store.store_event(datetime.strptime("2020-07-27 12:32:37.395438",'%Y-%m-%d %H:%M:%S.%f'),"Event2")
store.store_event(datetime.strptime("2020-07-27 12:14:25.983463",'%Y-%m-%d %H:%M:%S.%f'),"Event3")
store.store_event(datetime.now(),"Event4")
store.store_event(datetime.strptime("2020-07-27 12:30:37.395438",'%Y-%m-%d %H:%M:%S.%f'),"Event5")

print("All events, sorted by date")
for e in store.get_all_events():
    print(e)

print("\nEvents between 12:14 and 12:32 on 27/07/20, sorted by date")
end = datetime.strptime("2020-07-27 12:32:37",'%Y-%m-%d %H:%M:%S')
start = datetime.strptime("2020-07-27 12:14:25",'%Y-%m-%d %H:%M:%S')
for e in store.get_events(start,end):
    print(e)

print("\nUpdate Event 2")
print(store.get_event_by_id(2))
store.update_event_data(2,"Event666")
store.update_event_datetime(2,datetime.now())
print(store.get_event_by_id(2))

print("\nRemove events between 12:14 and 12:32 on 27/07/20")
store.remove_events(start,end)
for e in store.get_all_events():
    print(e)

store.drop()